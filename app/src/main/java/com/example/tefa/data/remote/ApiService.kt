package com.example.tefa.data.remote

import android.hardware.usb.UsbRequest
import com.example.tefa.data.remote.response.UserList
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Query

interface ApiService {
    @GET("api/users")
    suspend fun getUserList(
        @Query("page") page: Int,
    ): UserList.Response

}