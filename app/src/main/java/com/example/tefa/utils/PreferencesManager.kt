package com.example.tefa.utils

import android.content.Context
import android.content.SharedPreferences
import android.content.SharedPreferences.Editor

class PreferencesManager constructor(context: Context) {
    private  var pref: SharedPreferences
    private var editor: SharedPreferences.Editor

    init {
        pref = context.getSharedPreferences("AppSharedPref", Context.MODE_PRIVATE)
        editor = pref.edit()
        editor.apply()
    }
     var isFirstOpen: Boolean?
     get() {
         return pref.getBoolean("isDark",false)
     }

    set (status) {
        if (status != null) {
            editor.putBoolean("isDark", status)?.apply()
        }
    }
}